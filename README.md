# UltraWc

`npm run build:el`

`npm run start:lit`

`npm run start:vanilla`

`npm run start:re`

`npm run start:vu`

https://developer.mozilla.org/en-US/docs/Web/API/Web_components

https://lit.dev/

https://stenciljs.com/docs/introduction

https://angular.dev/

https://angular.io/

https://krausest.github.io/js-framework-benchmark/2024/table_chrome_123.0.6312.59.html

|                  | Speed | Size (gzip)                  | DX | Ecosystem | Features |
|------------------|-------|------------------------------|----|-----------|----------|
| Vanilla          | 4     | 4 (≈0.5kb)                   | 1  | 1         | 1        |
| Lit              | 3     | 3 (≈10kb)                    | 2  | 2         | 2        |
| Stencil.js       | 1     | 2 (≈28kb)                    | 3  | 2.5       | 3        |
| Angular Elements | 2.5   | 1 (≈42kb => ≈32kb(-zone.js)) | 4  | 4++       | 4+++     |

### Vanilla
##### Pros:
- Reference speed and size
##### Cons:
- simple things may be hard to do, medium+ things will be hard to do
- complex work with HTML
- integration with Angular will make things harder
- learning curve

### Lit
##### Pros:
- good speed and size
- Better DX
- optional shadow dom
- provides some built-in features (lifecycle hooks, decorators, etc...)
##### Cons:
- simple things may be hard to do, medium+ things will be hard to do
- small ecosystem
- integration with Angular will make things harder
- learning curve

### Stencil.js
##### Pros:
- Better DX
- optional shadow dom
- provides more built-in features (lifecycle hooks, form-associated components, etc...)
##### Cons:
- bad speed
- small ecosystem
- simple things may be hard to do
- learning curve

### Angular Elements
##### Pros:
- Our current stack
- Best DX
- good speed
- optional shadow dom
- framework: we have almost everything we need out of the box
- elements can be used as a components inside our applications 
##### Cons:
- compared to competitors bad bundle size
- some people still associate Angular with something not good, maybe Angular.js flashbacks (which was damn good for 2010-2013): but this is their's problem 😂
