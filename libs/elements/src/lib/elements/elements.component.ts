import { ChangeDetectionStrategy, ChangeDetectorRef, Component, inject, Input, ViewEncapsulation } from '@angular/core';
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'lib-elements',
  standalone: true,
  imports: [JsonPipe],
  templateUrl: './elements.component.html',
  styleUrl: './elements.component.css',
  // encapsulation: ViewEncapsulation.ShadowDom,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ElementsComponent {
  @Input() set data(data) {
    console.log('data', data);
    this._data = data;
  }
  get data() {
    return this._data;
  }
  private _data: any;
  @Input() type = 'angular-component';
  cdr = inject(ChangeDetectorRef);
  random: number | string = 1;
  constructor() {
    setInterval(() => {
      this.random = Math.random().toFixed(4);
      this.cdr.markForCheck();
    }, 1000);
  }
}
