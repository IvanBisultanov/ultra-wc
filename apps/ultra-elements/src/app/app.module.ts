import {
  CUSTOM_ELEMENTS_SCHEMA,
  DoBootstrap,
  inject,
  Injector,
  NgModule,
} from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { ElementsComponent } from '@ultra-wc/elements';

@NgModule({
  imports: [
    BrowserModule,
    ElementsComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule implements DoBootstrap {
  private injector = inject(Injector);

  ngDoBootstrap() {
    const changeEntityImage = createCustomElement(ElementsComponent, {injector: this.injector});
    customElements.define('angular-element', changeEntityImage);
  }
}
