console.log('Hello World');

customElements.define(
  "element-details",
  class extends HTMLElement {
    type: any;
    data: any;
    constructor() {
      super();
      const type = document.createElement("div");
      const data = document.createElement("div");
      data.textContent = `data=`;
      const shadowRoot = this.attachShadow({ mode: "open" });
      setInterval(() => {
        type.textContent = `${Math.random().toFixed(4)} [${this.type}]`;
        data.textContent = this.data ? `data=${JSON.stringify(this.data)}` : '';
        shadowRoot.innerHTML = '';
        shadowRoot.appendChild(type.cloneNode(true));
        shadowRoot.appendChild(data.cloneNode(true));
      }, 1000);
    }

    static get observedAttributes() { return ['type', 'data']; }
    attributeChangedCallback(attr, oldVal, newVal) {
      if (oldVal === newVal) return; // nothing to do
      switch (attr) {
        case 'type':
          this.type = newVal;
          break;
        case 'data':
          this.data = newVal;
          break;
      }
    }
  },
);
