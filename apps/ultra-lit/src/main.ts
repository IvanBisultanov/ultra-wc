import { LitElement, html, css } from 'lit';
import { customElement, property } from 'lit/decorators.js';


@customElement('lit-element')
export class MyElement extends LitElement {
  @property()
  random = Math.random().toFixed(4);
  @property()
  data;
  @property()
  type;

  static styles = css`
    p {
      margin: 0;
    }
  `;

  constructor() {
    super();
    setInterval(() => {
      this.random = Math.random().toFixed(4);
    }, 1000);
  }

  createRenderRoot() {
    return this;
  }

  render() {
    return html`
      <p>${this.random} [${ this.type }]</p>
      ${this.data ? `data=${JSON.stringify(this.data)}` : ''}
    `;
  }
}
