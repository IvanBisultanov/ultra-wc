/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 This is a starter component and can be deleted.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 Delete this file and get started with your project!
 * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
import { useEffect } from 'react';

declare global {
  namespace JSX {
    interface IntrinsicElements {
      'angular-element': any;
      'lit-element': any;
      'element-details': any;
    }
  }
}

export function NxWelcome({ title }: { title: string }) {
  useEffect(() => {
    setTimeout(() => {
      const itemsToUpdate = document.querySelectorAll('._data');
      console.log('itemsToUpdate', itemsToUpdate);
      itemsToUpdate.forEach((item, idx) => {
        (item as any).data = {
          random: Math.random() > .5 ? 'DATA+++++' : 'DATA-----',
        };
      });
    }, 1000);
  });
  return (
    <>
      <h4>WE ARE INSIDE REACT</h4>
      <br />

      <table>
        <tr>
          <th>Angular Element</th>
          <th>Lit</th>
          <th>JS</th>
        </tr>
        <tr>
          <td>
            <angular-element type="angular-element_inside-react" data={title}></angular-element>
            <angular-element type="angular-element_inside-react" class="_data"></angular-element>
            <angular-element type="angular-element_inside-react" class="_data"></angular-element>
            <angular-element type="angular-element_inside-react" class="_data"></angular-element>
          </td>
          <td>
            <lit-element type="lit-component_inside-react" data={title}></lit-element>
            <lit-element type="lit-component_inside-react" class="_data"></lit-element>
            <lit-element type="lit-component_inside-react" class="_data"></lit-element>
            <lit-element type="lit-component_inside-react" class="_data"></lit-element>
          </td>
          <td>
            <element-details type="vanilla-component_inside-react" data={title}></element-details>
            <element-details type="vanilla-component_inside-react" class="_data"></element-details>
            <element-details type="vanilla-component_inside-react" class="_data"></element-details>
            <element-details type="vanilla-component_inside-react" class="_data"></element-details>
          </td>
        </tr>
      </table>
    </>
  );
}

export default NxWelcome;
